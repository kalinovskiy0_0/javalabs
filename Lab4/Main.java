package org.example;

// Animal.java
public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

// Fish.java
public class Fish extends Animal {
    private String habitat;

    public Fish(String name, String habitat) {
        super(name);
        this.habitat = habitat;
    }

    public String getHabitat() {
        return habitat;
    }
}

// Amphibian.java
public class Amphibian extends Animal {
    private String skinType;

    public Amphibian(String name, String skinType) {
        super(name);
        this.skinType = skinType;
    }

    public String getSkinType() {
        return skinType;
    }
}

// Reptile.java
public class Reptile extends Animal {
    private String scales;

    public Reptile(String name, String scales) {
        super(name);
        this.scales = scales;
    }

    public String getScales() {
        return scales;
    }
}

// Bird.java
public class Bird extends Animal {
    private double wingspan;

    public Bird(String name, double wingspan) {
        super(name);
        this.wingspan = wingspan;
    }

    public double getWingspan() {
        return wingspan;
    }
}

// Mammal.java
public class Mammal extends Animal {
    private String furColor;

    public Mammal(String name, String furColor) {
        super(name);
        this.furColor = furColor;
    }

    public String getFurColor() {
        return furColor;
    }
}