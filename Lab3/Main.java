package org.example;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введіть стрічку:");
        String input = scanner.nextLine();

        char[] chars = input.toCharArray();

        Arrays.sort(chars);

        String sortedString = new String(chars);

        System.out.println("Відсортована стрічка: " + sortedString);

        scanner.close();
    }
}