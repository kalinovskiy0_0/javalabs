package ua.edu.tntu;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int size = 100;
        double[] cosValues = new double[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            double x = random.nextDouble();
            cosValues[i] = Math.cos(x * Math.PI * 2);
        }
        double maxCosValue = cosValues[0];
        for (int i = 1; i < size; i++) {
            if (cosValues[i] > maxCosValue) {
                maxCosValue = cosValues[i];
            }
        }
        System.out.println("Найбільший елемент масиву табличних значень функції cos(x): " + maxCosValue);
    }
}